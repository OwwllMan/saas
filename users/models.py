from django.db import models
from django.contrib.auth.models import User
from store.models import Store

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    user_track_max_store = models.IntegerField(default=0)
    user_track_store = models.IntegerField(default=0)
    user_subscription = models.CharField(max_length=100)
    user_subscription_price = models.FloatField(default=0)
    user_adress = models.CharField(max_length=255, default="Update your adress")
    user_country = models.CharField(max_length=255, default="Update your country")
    user_zipcode = models.CharField(max_length=50, default="Update your zipcode")
    user_society = models.CharField(max_length=100, default="Update your society name")

    def __str__(self):
        return f'{self.user.username} Profile'
