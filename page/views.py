from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from store.models import Store
from data.models import Data, Product, Variant
from datetime import date, datetime, timedelta



def home(request):
    date = datetime.today().strftime('%Y-%m-%d')
    today = datetime.strptime(date, '%Y-%m-%d')
    tomorrow = today + timedelta(days=1)

    orders = Data.objects.all()
    products = Product.objects.all()
    stores = Store.objects.all()

    added_stores_24h = Store.objects.filter(store_added_date__gte=today, store_added_date__lt=tomorrow)
    added_data_24h = Data.objects.filter(data_added_date__gte=today, data_added_date__lt=tomorrow)
    added_product_24h = Product.objects.filter(product_added_date__gte=today, product_added_date__lt=tomorrow)


    totalOrders = 0
    for order in orders:
        totalOrders += order.data_subtotal

    totalOrders24h = 0
    for order in added_data_24h:
        totalOrders24h += order.data_subtotal


    user = request.user
    context = {
    'stores': stores,
    'added_stores_24h': added_stores_24h,
    'totalOrders': totalOrders,
    'totalOrders24h': totalOrders24h,
    'products': products,
    'added_product_24h': added_product_24h,
    'user':user
    }
    return render(request, 'page/home.html', context)

def patch(request):
    context = {
    'title': "Patch note",
    }
    return render(request, 'page/pages/patch.html', context)

def legal(request):
    context = {
    'title': "Legal Notice",
    }
    return render(request, 'page/pages/legal.html', context)

def privacy(request):
    context = {
    'title': "Privacy Policy",
    }
    return render(request, 'page/pages/privacy.html', context)



@login_required
def dashboard(request):
    date = datetime.today().strftime('%Y-%m-%d')
    today = datetime.strptime(date, '%Y-%m-%d')
    tomorrow = today + timedelta(days=1)
    yesterday = today - timedelta(days=1)



    orders = Data.objects.all()
    products = Product.objects.all()
    stores = Store.objects.all()

    added_stores_24h = Store.objects.filter(store_added_date__gte=yesterday, store_added_date__lt=tomorrow)
    added_data_24h = Data.objects.filter(data_added_date__gte=yesterday, data_added_date__lt=tomorrow)
    added_product_24h = Product.objects.filter(product_added_date__gte=yesterday, product_added_date__lt=tomorrow)

    last3_products = Product.objects.all().order_by('-id')[:3]
    last3_stores = Store.objects.all().order_by('-id')[:3]

    last3_stores_products = []
    for store in last3_stores:
        products = Product.objects.filter(product_store=store)
        last3_stores_products.append(products)

    last3_stores_data = []
    for store in last3_stores:
        all_data = Data.objects.filter(data_store=store,data_added_date__gte=yesterday, data_added_date__lt=tomorrow)
        if  all_data:
            data_subtotal = 0
            for data in all_data:
                data_subtotal+= data.data_subtotal
            data_aov = data_subtotal/all_data.count()
            last3_stores_data.append([store.id,all_data.count(),data_aov])
        else:
            last3_stores_data.append([store.id,0,0])

    last3_products_variants = []
    for product in last3_products:
        variants = Variant.objects.filter(variant_product_id=product.product_id)
        last3_products_variants.append(variants)

    last3_products_data = []
    for product in last3_products:
        all_data = Data.objects.filter(data_product=product.product_id,data_added_date__gte=yesterday, data_added_date__lt=tomorrow)
        if  all_data:
            data_subtotal = 0
            for data in all_data:
                data_subtotal+= data.data_subtotal
            last3_products_data.append([product.product_id,all_data.count()])
        else:
            last3_products_data.append([product.product_id,0])


    totalOrders = 0
    for order in orders:
        totalOrders += order.data_subtotal

    totalOrders24h = 0
    for order in added_data_24h:
        totalOrders24h += order.data_subtotal


    user = request.user
    context = {
    'stores': stores,
    'added_stores_24h': added_stores_24h,
    'totalOrders': totalOrders,
    'totalOrders24h': totalOrders24h,
    'products': products,
    'added_product_24h': added_product_24h,
    'last3_products': last3_products,
    'last3_products_variants': last3_products_variants,
    'last3_products_data': last3_products_data,
    'last3_stores': last3_stores,
    'last3_stores_products': last3_stores_products,
    'last3_stores_data': last3_stores_data,
    'user':user,
    'title': 'Dashboard'
    }
    return render(request, 'page/dashboard.html', context)

def trendingtrack(request, pk):
    store = Store.objects.get(pk=pk)
    if  request.user in store.store_users.all():
        messages.success(request, f''+store.store_name+' already added')
        return redirect('store-list')
    else:
        store.store_users.add(request.user)
        profile = request.user.profile
        profile.user_track_store -=1
        messages.success(request, f''+store.store_name+' added')
        return redirect('store-list')
    

def custom_page_not_found_view(request, exception):
    return render(request, "page/errors/404.html", {'title': "Error 404"})

def custom_error_view(request, exception=None):
    return render(request, "page/errors/500.html", {'title': "Error 500"})

def custom_permission_denied_view(request, exception=None):
    return render(request, "page/errors/403.html", {'title': "Error 403"})

def custom_bad_request_view(request, exception=None):
    return render(request, "page/errors/400.html", {'title': "Error 400"})