from django.urls import path
from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('add_trending_tracking/<int:pk>/', views.trendingtrack, name='add_trending_tracking'),
    path('patch/', views.patch, name='patch'),
    path('legal-notice/', views.legal, name='legal'),
    path('privacy-policy/', views.privacy, name='privacy'),

]
