from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from users import views as user_views

from django.conf import settings#DEV FOR STATIC FILE
from django.conf.urls.static import static#DEV FOR STATIC FILE


from django.conf.urls import (
handler400, handler403, handler404, handler500
)

urlpatterns = [
    path('', include('page.urls')),
    path('', include('store.urls')),
    path('', include('subscription.urls')),

    path('admin/', admin.site.urls),

    path('profile/', user_views.profile, name='profile'),
    path('profile/update/', user_views.profile_update, name='profile-update'),

    path('register/', user_views.register, name='register'),
    path('login/', auth_views.LoginView.as_view(template_name="users/login.html"), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name="users/logout.html"),{'next_page': settings.LOGOUT_REDIRECT_URL}, name='logout'),
]


handler404 = 'page.views.custom_page_not_found_view'
handler500 = 'page.views.custom_error_view'
handler403 = 'page.views.custom_permission_denied_view'
handler400 = 'page.views.custom_bad_request_view'