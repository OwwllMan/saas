from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse

# Create your models here.

class Store(models.Model):
    store_users = models.ManyToManyField(User, blank=True)
    store_name = models.CharField(max_length=100)
    store_url = models.CharField(max_length=100)
    store_icon = models.CharField(max_length=100)
    store_category = models.CharField(max_length=100)
    store_lang = models.CharField(max_length=100)
    store_theme = models.CharField(max_length=100)
    store_added_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.store_name

    def get_absolute_url(self):
        return reverse('store-list')
