from datetime import datetime, timedelta
from urllib import request
import requests
import favicon

from django.shortcuts import redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.shortcuts import render
from django.views.generic import (CreateView, DeleteView, DetailView, ListView,
                                  UpdateView)

from data.models import Data, Product, Variant

from .models import Store
from users.models import User

class StoreListView(LoginRequiredMixin, ListView):
    model = Store
    context_object_name = 'stores'
    ordering = ['-store_added_date']
    

    def get_context_data(self, **kwargs):
        context = super(StoreListView, self).get_context_data(**kwargs)
        user_stores = Store.objects.filter(store_users=self.request.user)

        user_stores_last24h = []
        for store in user_stores:
            actualh = 0
            totalh = 24
            subtotalorder24h = 0
            dateh = datetime.today().strftime('%Y-%m-%d %H')
            hournow = datetime.strptime(dateh, '%Y-%m-%d %H')
            nexthour = hournow + timedelta(hours=1)
            last24h = []
            subtotalorderbyhour = []
            while totalh > actualh:
                h = hournow - timedelta(hours=actualh)
                hstring = str(h.strftime("%H"))
                last24h.append([hstring])
                orderlast24h =  Data.objects.filter(data_store=store, data_added_date__gte=h, data_added_date__lt=nexthour)
                ordersubtotalh = 0
                for order in orderlast24h:
                    subtotalorder24h += order.data_subtotal
                    ordersubtotalh+= order.data_subtotal
                subtotalorderbyhour.append(ordersubtotalh)
                actualh+=1
            user_stores_last24h.append([subtotalorder24h, store.id])

        context['user_stores'] = user_stores
        context['user_stores_last24h'] = user_stores_last24h
        context['title'] = "List of stores"
        return context

class StoreDetailView(LoginRequiredMixin,UserPassesTestMixin, DetailView):
    model = Store
    

    def get_context_data(self, **kwargs):
        context = super(StoreDetailView, self).get_context_data(**kwargs)
        current_store = Store.objects.get(pk=self.kwargs.get("pk"))
        store_data = Data.objects.filter(data_store=current_store)
        products = Product.objects.filter(product_store=current_store)
        variants = Variant.objects.filter(variant_store=current_store)
        last5order = Data.objects.filter(data_store=current_store).order_by('-id')[:5]
        storeorders = Data.objects.filter(data_store=current_store)

        last5orderformat = []
        for order in last5order:
            orderinfo = []
            order.data_product
            productoforder = Product.objects.get(product_store=current_store, product_id=order.data_product)
            orderinfo.append(productoforder.product_title)
            orderinfo.append(productoforder.product_image)
            orderinfo.append(productoforder.product_handle)
            orderinfo.append(order.data_subtotal)
            orderinfo.append(order.data_added_date)
            last5orderformat.append(orderinfo)

        #orderlast7h =  Data.objects.filter(store=store, data_added_date__gte=start_of_week, data_added_date__lt=end_of_week)
        

        orderdateh = datetime.today().strftime('%Y-%m-%d %H')
        orderhournow = datetime.strptime(orderdateh, '%Y-%m-%d %H')
        orderh = orderhournow - timedelta(hours=23)

        orderdated = datetime.today().strftime('%Y-%m-%d')
        orderdnow = datetime.strptime(orderdated, '%Y-%m-%d')
        orderd = orderdnow - timedelta(days=7)


        #pagination for product list 
        page_num = self.request.GET.get('page', 1)
        paginator = Paginator(products, 10)
        try:
            products_data = paginator.page(page_num)
        except PageNotAnInteger:
            # if page is not an integer, deliver the first page
            products_data = paginator.page(1)
        except EmptyPage:
            # if the page is out of range, deliver the last page
            products_data = paginator.page(paginator.num_pages)

        #total ca of the store
        total_store = 0
        for d in storeorders:
            total_store += d.data_subtotal

        #graph total ca last 24hrs
        actualh = 0
        totalh = 24
        subtotalorder24h = 0
        dateh = datetime.today().strftime('%Y-%m-%d %H')
        hournow = datetime.strptime(dateh, '%Y-%m-%d %H')
        nexthour = hournow + timedelta(hours=1)
        last24h = []
        subtotalorderbyhour = []
        while totalh > actualh:
            h = hournow - timedelta(hours=actualh)
            hstring = str(h.strftime("%H"))
            last24h.append([hstring])
            orderlast24h =  Data.objects.filter(data_store=current_store, data_added_date__gte=h, data_added_date__lt=nexthour)
            ordersubtotalh = 0
            for order in orderlast24h:
                subtotalorder24h += order.data_subtotal
                ordersubtotalh+= order.data_subtotal
            subtotalorderbyhour.append(ordersubtotalh)
            actualh+=1


        #graph total ca last 7day
        actuald = 0
        totald = 7
        subtotalorder7day = 0
        dated = datetime.today().strftime('%Y-%m-%d')
        daynow = datetime.strptime(dated, '%Y-%m-%d')
        tomorrow = daynow + timedelta(days=1)
        last7d = []
        subtotalorderbyday = []
        while totald > actuald:
            d = daynow - timedelta(days=actuald)
            dstring = str(d.strftime("%m-%d"))
            last7d.append([dstring])
            orderlast7d =  Data.objects.filter(data_store=current_store, data_added_date__gte=d, data_added_date__lt=tomorrow)
            ordersubtotalday = 0
            for order in orderlast7d:
                subtotalorder7day += order.data_subtotal
                ordersubtotalday+= order.data_subtotal
            subtotalorderbyday.append(ordersubtotalday)
            actuald+=1

        context['subtotalorder7day'] = subtotalorder7day
        context['subtotalorder24h'] = subtotalorder24h
        context['subtotalorderbyhour'] = subtotalorderbyhour
        context['subtotalorderbyday'] = subtotalorderbyday
        context['last7d'] = last7d
        context['last24h'] = last24h
        context['title'] = current_store.store_name
        context['total_store'] = total_store
        context['last5orderformat'] = last5orderformat
        context['products_data'] = products_data
        context['store'] = current_store
        context['products'] = Product.objects.filter(product_store=current_store)
        context['variants'] = Variant.objects.filter(variant_store=current_store)
        return context

    def test_func(self):
        store = self.get_object()
        print(store.store_users.all)
        if self.request.user in store.store_users.all():
            return True
        return False

class StoreCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = Store
    fields = ['store_name', 'store_url']

    def get_context_data(self, **kwargs):
        context = super(StoreCreateView, self).get_context_data(**kwargs)
        context['title'] = "Track new store"
        return context


    def form_valid(self, form):
        existstore = Store.objects.filter(store_url=form.instance.store_url)
        if existstore:
            store = Store.objects.get(store_url=form.instance.store_url)
            if self.request.user in store.store_users.all():
                messages.success(self.request, f''+store.store_name+' already added')
                return redirect('store-list')
            else:
                store.store_users.add(self.request.user)
                profile = self.request.user.profile
                profile.user_track_store +=1
                profile.save()
                messages.success(self.request, f''+store.store_name+' added')
                return redirect('store-list')
        else:
            form.instance.store_added_date = datetime.today().strftime('%Y-%m-%d')
            icons = favicon.get('https://'+form.instance.store_url)
            icon = icons[0]
            form.instance.store_icon = icon.url
            profile = self.request.user.profile
            profile.user_track_store +=1
            profile.save()
            messages.success(self.request,  f''+form.instance.store_name+' added')
            super().form_valid(form)
            store = Store.objects.get(store_url=form.instance.store_url)
            store.store_users.add(self.request.user)
            return redirect('store-list')

    def test_func(self):
        if self.request.user.profile.user_track_store < self.request.user.profile.user_track_max_store:
            return True
        return False

class StoreUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = Store
    fields = ['store_name', 'store_url']

    def form_valid(self, form):
        form.instance.store_user = self.request.user
        return super().form_valid(form)

    def test_func(self):
        store = self.get_object()
        if self.request.user == store.store_users.all:
            return True
        return False

def delete_tracking(request, pk):
    store = Store.objects.get(pk=pk)
    store.store_users.remove(request.user)
    profile = request.user.profile
    profile.user_track_store -=1
    messages.success(request, f'Store remove')
    return redirect('store-list')


class StoreDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = Store
    success_url = '/stores/'

    def test_func(self):
        return False
