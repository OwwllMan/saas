from django.urls import path
from django.urls import path, include

from .views import StoreListView, StoreDetailView, StoreCreateView, StoreUpdateView, StoreDeleteView
from . import views

urlpatterns = [
    path('stores/', StoreListView.as_view(), name='store-list'),
    path('stores/<int:pk>/', StoreDetailView.as_view(), name='store-detail'),
    path('stores/new/', StoreCreateView.as_view(), name='store-create'),
    path('stores/<int:pk>/update/', StoreUpdateView.as_view(), name='store-update'),
    path('stores/<int:pk>/delete/', StoreDeleteView.as_view(), name='store-delete'),
    path('delete_tracking/<int:pk>/', views.delete_tracking, name='delete_tracking'),
]
