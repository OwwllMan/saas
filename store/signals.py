from django.db.models.signals import post_save
from django.dispatch import receiver
from store.models import Store
from data.models import Product, Variant
import requests
from datetime import datetime
from dateutil.parser import parse
import pytz

@receiver(post_save, sender=Store)
def addstore(sender, instance, **kwargs):
    URL = "https://"+instance.store_url+"/products.json"
    r = requests.get(URL)
    request = r.json()

    NbProducts = len(request['products'])
    ScrapProduct = 0
    while NbProducts > ScrapProduct:
        product = request['products'][ScrapProduct]
        product_image = request['products'][ScrapProduct]['images'][0]["src"]
        product_price = request['products'][ScrapProduct]['variants'][0]["price"]
        product_id = request['products'][ScrapProduct]['id']
        product_title = request['products'][ScrapProduct]['title']
        product_handle = request['products'][ScrapProduct]['handle']
        product_updated_at = request['products'][ScrapProduct]['updated_at']
        product_created_at = request['products'][ScrapProduct]['created_at']
        NbVariants = len(request['products'][ScrapProduct]['variants'])
        ScrapProduct +=1
        product_new_format = parse(product_updated_at)
        product_new = product_new_format.replace(tzinfo=pytz.UTC)
        product_new_created_format = parse(product_created_at)
        product_new_created = product_new_created_format.replace(tzinfo=pytz.UTC)
        productobj = Product(product_store=instance, product_id=product_id,product_image=product_image,product_price=product_price,product_title=product_title,product_handle=product_handle,product_updated_at=product_new,product_created_at=product_new_created)
        productobj.save()
        ScrapVariant = 0
        while NbVariants > ScrapVariant:
            variant = product['variants'][ScrapVariant]
            variant_id = variant["id"]
            variant_product_id = variant["product_id"]
            variant_title = variant["title"]
            variant_price = variant["price"]
            variant_compared_price = variant["compare_at_price"]
            variant_updated_at = variant["updated_at"]
            variant_created_at = variant["created_at"]
            ScrapVariant += 1
            variant_new_format = parse(variant_updated_at)
            variant_new = variant_new_format.replace(tzinfo=pytz.UTC)
            variant_new_created_format = parse(variant_created_at)
            variant_new_created = variant_new_created_format.replace(tzinfo=pytz.UTC)
            variantobj = Variant(variant_store=instance, variant_id=variant_id,variant_product_id=variant_product_id,variant_title=variant_title,variant_price=variant_price,variant_updated_at=variant_new,variant_created_at=variant_new_created)
            variantobj.save()
            if NbVariants == ScrapVariant:
                break
