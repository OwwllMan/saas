from django.db import models
from django.contrib.auth.models import User


class Subscription(models.Model):
    name = models.CharField(max_length=100)
    stripe_product_id = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Price(models.Model):
    subscription = models.ForeignKey(Subscription, on_delete=models.CASCADE)
    stripe_price_id = models.CharField(max_length=100)
    price = models.IntegerField(default=0)  # cents

class Order(models.Model):
    order_by = models.ForeignKey(User, on_delete=models.CASCADE)
    order_subscription = models.CharField(max_length=50)
    order_price = models.IntegerField(default=0)
    order_date = models.DateTimeField(auto_now=True)

    def get_display_price(self):
        return "{0:.2f}".format(self.price / 100)
