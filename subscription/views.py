import stripe
from django.conf import settings
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.views.generic import TemplateView
from django.views import View
from django.contrib.auth.models import User
from .models import Subscription, Order, Price
from users.models import Profile

stripe.api_key = settings.STRIPE_SECRET_KEY

class SubscriptionPricingPageView(TemplateView):
    template_name = "subscription/pricing.html"

    def get_context_data(self, **kwargs):
        subscription = Subscription.objects.get(name="TraklySubscription")
        prices = Price.objects.filter(subscription=subscription)
        context = super(SubscriptionPricingPageView,
                        self).get_context_data(**kwargs)
        context.update({
            "title": "Pricing",
            "subscription": subscription,
            "prices": prices
        })
        return context

class CreateCheckoutSessionView(View):
    def post(self, request, *args, **kwargs):
        price = Price.objects.get(id=self.kwargs["pk"])
        user = self.request.user
        YOUR_DOMAIN = "https://trakly.herokuapp.com/"  # change in production
        checkout_session = stripe.checkout.Session.create(
            payment_method_types=['card'],
            line_items=[
                {
                    'price': price.stripe_price_id,
                    'quantity': 1,
                },
            ],
            mode='subscription',
            customer_email= user.email,
            success_url=YOUR_DOMAIN + '/success/',
            cancel_url=YOUR_DOMAIN + '/cancel/',
        )
        return redirect(checkout_session.url)


class SuccessView(TemplateView):
    template_name = "subscription/success.html"

    def get_context_data(self, **kwargs):
        user = self.request.user
        last_order = Order.objects.filter(order_by=user).last()
        context = super(SuccessView,self).get_context_data(**kwargs)
        context.update({
            "title": "Order complete",
            "order": last_order,
        })
        return context

class CancelView(TemplateView):
    template_name = "subscription/cancel.html"
    def get_context_data(self, **kwargs):
        context = super(CancelView,self).get_context_data(**kwargs)
        context.update({
            "title": "Order not complete",
        })
        return context


@csrf_exempt
def stripe_webhook(request):
    payload = request.body
    sig_header = request.META['HTTP_STRIPE_SIGNATURE']
    event = None

    try:
        event = stripe.Webhook.construct_event(
            payload, sig_header, settings.STRIPE_WEBHOOK_SECRET
        )
    except ValueError as e:
        # Invalid payload
        return HttpResponse(status=400)
    except stripe.error.SignatureVerificationError as e:
        # Invalid signature
        return HttpResponse(status=400)

    # Handle the checkout.session.completed event
    if event['type'] == 'checkout.session.completed':
        session = event['data']['object']
        customer_email = session["customer_details"]["email"]
        payment_intent = session["payment_intent"]
        user = User.objects.get(email=customer_email)
        line_items = stripe.checkout.Session.list_line_items(session["id"])
        stripe_price_id = line_items["data"][0]["price"]["id"]
        price = Price.objects.get(stripe_price_id=stripe_price_id)
        product = price.product
        if price.price == 4900:
            print("Subscription small plan")
            order = Order.objects.create(order_by=user, order_product=product, order_price=price.price)
            order.save()
            user.profile.user_subscription_price = 49
            user.profile.user_subscription = "small"
            user.profile.user_track_max_store = 30
            user.profile.save()
        elif price.price == 8900:
            print("Subscription medium plan")
            order = Order.objects.create(order_by=user, order_product=product, order_price=price.price)
            order.save()
            user.profile.user_subscription_price = 89
            user.profile.user_subscription = "medium"
            user.profile.user_track_max_store = 60
            user.profile.save()
        elif price.price == 13900:
            print("Subscription pro plan")
            order = Order.objects.create(order_by=user, order_product=product, order_price=price.price)
            order.save()
            user.profile.user_subscription_price = 139
            user.profile.user_subscription = "pro"
            user.profile.user_track_max_store = 100
            user.profile.save()
        else:
            print("Error")
        print("Order complete")
        # TODO - send an email to the customer

    return HttpResponse(status=200)
