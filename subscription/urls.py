from django.urls import path
from subscription.views import (
    CreateCheckoutSessionView,
    SuccessView,
    CancelView,
    SubscriptionPricingPageView,
    stripe_webhook,
)

urlpatterns = [
    path('pricing/', SubscriptionPricingPageView.as_view(), name='pricing'),
    path('cancel/', CancelView.as_view(), name='cancel'),
    path('success/', SuccessView.as_view(), name='success'),
    path('create-checkout-session/<pk>/', CreateCheckoutSessionView.as_view(), name='create-checkout-session'),
    path('webhooks/stripe/', stripe_webhook, name='stripe-webhook'),
]
