from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.urls import reverse
from store.models import Store
# Create your models here.

class Data(models.Model):
    data_store = models.ForeignKey(Store, on_delete=models.CASCADE)
    data_date = models.DateTimeField(default=timezone.now)
    data_product = models.CharField(max_length=100)
    data_subtotal = models.FloatField(default=0)
    data_added_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.data_product


class Product(models.Model):
    product_store = models.ForeignKey(Store, on_delete=models.CASCADE)
    product_id = models.BigIntegerField()
    product_image = models.CharField(max_length=255)
    product_price = models.FloatField(default=0)
    product_title = models.CharField(max_length=100)
    product_handle = models.CharField(max_length=100)
    product_created_at = models.DateTimeField()
    product_updated_at = models.DateTimeField()
    product_added_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.product_title


class Variant(models.Model):
    variant_store = models.ForeignKey(Store, on_delete=models.CASCADE)
    variant_id = models.BigIntegerField()
    variant_product_id = models.BigIntegerField()
    variant_title = models.CharField(max_length=100)
    variant_price = models.FloatField(default=0)
    variant_price_compared = models.FloatField(default=0)
    variant_updated_at =  models.DateTimeField()
    variant_created_at = models.DateTimeField()
    variant_added_date = models.DateTimeField(default=timezone.now)


    def __str__(self):
        return self.variant_title
