from datetime import datetime
from apscheduler.schedulers.background import BackgroundScheduler
from .event import getting_event
import time



def start_getting_event():
	scheduler = BackgroundScheduler()
	scheduler.add_job(getting_event, 'interval', hours=10)
	scheduler.print_jobs()
	scheduler.start()
