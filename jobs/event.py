from django.conf import settings
from apscheduler.schedulers.background import BackgroundScheduler
from store.models import Store
from data.models import Data, Product, Variant
import requests

from datetime import datetime
from dateutil.parser import parse
import pytz

def getting_event():
    stores = Store.objects.all()
    for store in stores:
        URL = "https://"+store.store_url+"/products.json"
        r = requests.get(URL)
        request = r.json()
        NbProducts = len(request['products'])
        print("Checking event for",store.store_name,'...' )
        ScrapProduct = 0
        while NbProducts > ScrapProduct:
            product = request['products'][ScrapProduct]
            product_id = request['products'][ScrapProduct]['id']
            product_title = request['products'][ScrapProduct]['title']
            product_handle = request['products'][ScrapProduct]['handle']
            product_updated_at = request['products'][ScrapProduct]['updated_at']
            if Product.objects.filter(product_id=product_id).exists():
                p=""
            else:
                productobj = Product(product_store=store, product_id=product_id,product_title=product_title,product_handle=product_handle,product_updated_at=product_updated_at)
                productobj.save()

            variants = Variant.objects.filter(variant_store=store)
            NbVariants = len(request['products'][ScrapProduct]['variants'])
            ScrapProduct +=1
            ScrapVariant = 0
            while NbVariants > ScrapVariant:
                variant = product['variants'][ScrapVariant]
                variant_id = variant["id"]
                variant_product_id = variant["product_id"]
                variant_title = variant["title"]
                variant_price = variant["price"]
                variant_compared_price = variant["compare_at_price"]
                variant__updated_at = variant["updated_at"]
                variant_check = Variant.objects.get(variant_id=variant_id)
                if variant_check:
                    variant_db = variant_check.variant_updated_at.replace(tzinfo=pytz.UTC)
                    variant_new_format = parse(variant__updated_at)
                    variant_new = variant_new_format.replace(tzinfo=pytz.UTC)

                    if variant_db != variant_new:
                        variant_update = Variant.objects.get(variant_id=variant_id)
                        variant_update.variant_updated_at = variant_new
                        variant_update.save()
                        print("Variant update ",variant_update.variant_updated_at)
                        dataobj = Data(data_store=store,data_date=variant_update.variant_updated_at,data_product=product_id,data_subtotal=variant_price)
                        dataobj.save()
                    else:
                        p=""
                else:
                    variantobj = Variant(variant_store=instance, variant_id=variant_id,variant_product_id=variant_product_id,variant_title=variant_title,variant_price=variant_price,variant_updated_at=variant_updated_at)
                    variantobj.save()
                ScrapVariant += 1
                if NbVariants == ScrapVariant:
                    break
